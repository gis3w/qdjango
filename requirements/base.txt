# Base requirements
django==1.6.7
django-autoslug==1.6.1
django-braces==0.2.3
django-crispy-forms==1.4.0
django-extensions==1.1.1
django-model-utils==1.3.0
dj-database-url==0.2.1
psycopg2==2.5
South==0.7.6
lxml==3.1.1
defusedxml==0.4.1
Pillow==2.0.0
SQLAlchemy==0.8.1
django-guardian
