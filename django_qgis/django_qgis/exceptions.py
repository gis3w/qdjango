from django.shortcuts import render_to_response
from django.template import RequestContext

def render_error(msg,request):
    # this trick is to solve the problem that RequestContext doesn't cast SimpleLazyObject around
    # the user instance, when called from FormView. This call causes the the lazy object _setup to 
    # be executed correctly
    __dummy__ = request.user.is_authenticated()
    data = {'error':{'msg':msg}}
    return render_to_response('generic_error.html',data,context_instance=RequestContext(request))