# settings/base.py
# See https://github.com/twoscoops/django-twoscoops-project
from os import environ
from os.path import abspath, basename, dirname, join, normpath
from sys import path

# Do not import anything from Django directly into your settings,
# ImproperlyConfigured is an exception
from django.core.exceptions import ImproperlyConfigured
import dj_database_url


########## ENVIRONMENT VARIABLE HELPER FUNCTION
def get_env_variable(var_name):
    """Get the environment variable or return exception."""

    try:
        return environ[var_name]
    except KeyError:
        error_msg = "Set the %s environment variable" % var_name
        raise ImproperlyConfigured(error_msg)
########## END ENVIRONMENT VARIABLE HELPER FUNCTION


########## PATH CONFIGURATION
# Absolute filesystem path to the Django project directory:
DJANGO_ROOT = dirname(dirname(abspath(__file__)))

# Absolute filesystem path to the top-level project folder:
SITE_ROOT = dirname(DJANGO_ROOT)

# Site name:
SITE_NAME = basename(DJANGO_ROOT)

# Add our project to our pythonpath, this way we don't need to type our project
# name in our dotted import paths:
path.append(DJANGO_ROOT)
########## END PATH CONFIGURATION


########## DEBUG CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#debug
DEBUG = False

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#template-debug
TEMPLATE_DEBUG = DEBUG
########## END DEBUG CONFIGURATION


########## DATABASE CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#databases
#DATABASES = {'default': dj_database_url.parse(get_env_variable('DATABASE'))}

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.spatialite', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'djangoqgis',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}
########## END DATABASE CONFIGURATION


########## GENERAL CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#time-zone
TIME_ZONE = 'Europe/Rome'

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#language-code
LANGUAGE_CODE = 'en-US'

#LOCALE_PATHS = join(DJANGO_ROOT, 'locale')

gettext = lambda s: s
LANGUAGES = (
    ('it', gettext('Italian')),
    ('en', gettext('English')),
)

LOCALE_PATHS = (join(SITE_ROOT,'locale'),)

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#use-tz
USE_TZ = True
########## END GENERAL CONFIGURATION


########## MEDIA CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#media-root
# MEDIA_ROOT = normpath(join(dirname(dirname(SITE_ROOT)), '%s-media' % SITE_NAME))
MEDIA_ROOT = ''

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#media-url
MEDIA_URL = '/media/'
########## END MEDIA CONFIGURATION

LOGOUT_NEXT_PAGE = LOGIN_URL = '/login/'



########## STATIC FILE CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#static-root
# STATIC_ROOT = normpath(join(dirname(dirname(SITE_ROOT)), '%s-static' % SITE_NAME))
STATIC_ROOT = ''

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#static-url
STATIC_URL = '/static/'

# See: https://docs.djangoproject.com/en/1.5/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    normpath(join(DJANGO_ROOT, 'static')),
)

# See: https://docs.djangoproject.com/en/1.5/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
########## END STATIC FILE CONFIGURATION


########## SECRET CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#secret-key
# Note: This key only used for development and testing.
SECRET_KEY = 'some-stuff-here'
########## END SECRET CONFIGURATION


########## FIXTURE CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    normpath(join(DJANGO_ROOT, 'fixtures')),
)
########## END FIXTURE CONFIGURATION


########## TEMPLATE CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#template-context-processors
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',
    'django_qgis.context_processors.global_settings',
)

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#template-loaders
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#template-dirs
TEMPLATE_DIRS = (
    normpath(join(DJANGO_ROOT, 'templates')),
)
########## END TEMPLATE CONFIGURATION


########## MIDDLEWARE CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#middleware-classes
MIDDLEWARE_CLASSES = (
    # Default Django middleware.
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)
########## END MIDDLEWARE CONFIGURATION


########## URL CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#root-urlconf
ROOT_URLCONF = '%s.urls' % SITE_NAME
########## END URL CONFIGURATION


########## APP CONFIGURATION
DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Useful template tags:
    # 'django.contrib.humanize',

    # Admin panel and documentation:
    'django.contrib.admin',
    'django.contrib.admindocs',
)

THIRD_PARTY_APPS = (
    'south',
    'crispy_forms',
    'guardian',
)

# Apps specific for this project go here.
LOCAL_APPS = (
    'django_qgis_admin',
    'projects',
)

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS
########## END APP CONFIGURATION

########## DJANGO-GUARDIAN SETTIGNS
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend', # this is default
)

ANONYMOUS_USER_ID = -1
########## END DJANGO-GUARDIAN SETTIGNS

########## INSTALLED APPS SETTINGS
# Projects app, see ``projects/utils/project_import.py``
DATASOURCE_PATH = ''
########## END INSTALLED APPS SETTINGS


########## LOGGING CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#logging
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
########## END LOGGING CONFIGURATION
CRISPY_TEMPLATE_PACK = 'bootstrap3'

########## WSGI CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#wsgi-application
WSGI_APPLICATION = 'wsgi.application'
########## END WSGI CONFIGURATION

VERSION = '0.8'
