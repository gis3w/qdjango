# Production settings and globals.
from os import environ

from .base import *

########## EMAIL CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#email-backend
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#email-host
EMAIL_HOST = environ.get('EMAIL_HOST', 'smtp.gmail.com')

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#email-host-password
EMAIL_HOST_PASSWORD = environ.get('EMAIL_HOST_PASSWORD', '')

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#email-host-user
EMAIL_HOST_USER = environ.get('EMAIL_HOST_USER', 'your_email@example.com')

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#email-port
EMAIL_PORT = environ.get('EMAIL_PORT', 587)

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#email-subject-prefix
EMAIL_SUBJECT_PREFIX = '[%s] ' % SITE_NAME

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#email-use-tls
EMAIL_USE_TLS = True

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#server-email
SERVER_EMAIL = EMAIL_HOST_USER
########## END EMAIL CONFIGURATION

ADMIN_NAME = 'admin'
ADMIN_EMAIL = 'admin@example.org'

########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#caches
# CACHES = {}
########## END CACHE CONFIGURATION

########### TO ADD GLOBAL LOGIN 
MIDDLEWARE_CLASSES += (
    #'django_qgis.middlewares.loginrequiredmiddleware.LoginRequiredMiddleware',
)
LOGIN_EXEMPT_URLS = ('admin/','login')
################################

########### TIME AND LANGUAGE CONFIG (USED BY i18)
TIME_ZONE='Europe/Rome'
LANGUAGE_CODE = 'en'
################################

########### GLOBAL URL PREFIX OF THE APPLICATION ROOT (ex. http://localhost/djangoqgis "/djangoqgis/" SHOULD BE SET)
URL_PREFIX = ''
################################

########## MEDIA AND STATIC CONFIGURATION
#MEDIA_ROOT = get_env_variable('MEDIA_ROOT')
#STATIC_ROOT = get_env_variable('STATIC_ROOT')
########## END MEDIA AND STATIC CONFIGURATION

from local_settings import *

LOGIN_URL = SITE_URL_PREFIX+'/login/'
LOGIN_REDIRECT_URL = SITE_URL_PREFIX
LOGOUT_NEXT_PAGE = SITE_URL_PREFIX + '/'

MIDDLEWARE_CLASSES += LOCAL_MIDDLEWARE_CLASSES

DATABASES = {'default': dj_database_url.parse(DATABASE)}

########## MANAGER CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#admins
ADMINS = (ADMIN_NAME, ADMIN_EMAIL,)

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#managers
MANAGERS = ADMINS
########## END MANAGER CONFIGURATION

########## ALLOWED HOSTS
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = HOST_LIST
########## END ALLOWED HOSTS
