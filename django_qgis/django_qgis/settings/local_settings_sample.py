########## DEBUG CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#debug
DEBUG = False

# See: https://docs.djangoproject.com/en/1.5/ref/settings/#template-debug

########## END DEBUG CONFIGURATION

ADMIN_NAME = 'XXX YYY'
ADMIN_EMAIL = 'info@example.com'


########## EMAIL CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#email-backend
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
########## END EMAIL CONFIGURATION


########## CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/1.5/ref/settings/#caches
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}
########## END CACHE CONFIGURATION

DATABASE = 'postgres://user:pssword@localhost/djangoqgis'

LOCAL_MIDDLEWARE_CLASSES = (
    #'django_qgis.middlewares.loginrequiredmiddleware.LoginRequiredMiddleware',
)
LOGIN_EXEMPT_URLS = ('admin/','login')

TIME_ZONE='Europe/Rome'
LANGUAGE_CODE = 'it'

MEDIA_ROOT = '/var/www/djangoqgis/media/'
MEDIA_URL = '/djqgis_media/'
STATIC_ROOT = '/var/www/djangoqgis/static/'
STATIC_URL = '/djqgis_static/'
DATASOURCE_PATH = '/var/www/djangoqgis/media/dati_geografici/'

HOST_LIST = ['localhost']
SITE_URL_PREFIX = '/djqgis'