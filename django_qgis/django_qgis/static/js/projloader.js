var mapSearchPanelConfigs = {};
DjangoQGIS.reloadingpanels = false;

DjangoQGIS.setProjectWidgetsOptions = function(data){
	DjangoQGIS.setTooltipOptions(data);
	DjangoQGIS.setSearchOptions(data);
};

DjangoQGIS.setTooltipOptions = function(data){
	var tooltips = data['widgets']['tooltip'];
	tooltipTemplates = {};
	for (var i=0; i<tooltips.length; i++){
		var tooltip = tooltips[i];
		var tooltip_template = '';
		tooltipTemplates[tooltip['layer']] = {'template':''};
		var tooltip_txt_template = '';
		for (var k=0;k<tooltip['options'].length;k++){
			var options = tooltip['options'][k];
			if (options['image']==true){
                image_path = options['field'];
                field_template = "<%"+image_path+"%>";
                if (!/http:\/\/.+\..+/i.test(image_path)){
                    field_template = "/"+field_template;
                }
				tooltip_template += options['text']+': <br><img src='+field_template+' width='+options['img_width']+' height='+options['img_height']+'/><br>';
			}
			else{
				tooltip_template += options['text']+': <%'+options['field']+'%><br>';
			}
		}
		tooltipTemplates[tooltip['layer']]['template'] = tooltip_template;
	}
}

DjangoQGIS.setSearchOptions = function(data){
	mapSearchPanelConfigs[data['wmsMapName']+'.qgs'] = data['widgets']['search'];
	
	if (DjangoQGIS.reloadingpanels) {
		var searchPanelConfigs = [];
		
			if (wmsMapName in mapSearchPanelConfigs || wmsMapName+'.qgs' in mapSearchPanelConfigs) {
				searchPanelConfigs = mapSearchPanelConfigs[wmsMapName];
			}
			if (wmsMapName+'.qgs' in mapSearchPanelConfigs) {
				searchPanelConfigs = mapSearchPanelConfigs[wmsMapName+'.qgs'];
			}
			if (searchPanelConfigs.length > 0) {
				// add QGIS search panels
				var searchPanel = Ext.getCmp('SearchPanel');
				searchPanel.removeAll();
				var searchTabPanel = Ext.getCmp('SearchTabPanel');
				if (typeof searchTabPanel == 'undefined'){
					searchPanel.show();
					searchPanel.insert(0,{
								xtype: 'tabpanel',
								enableTabScroll: true,
								activeTab: 0,
								id: 'SearchTabPanel',
								items: []
							});
					searchPanel.doLayout();
					searchTabPanel = Ext.getCmp('SearchTabPanel');
				}
				for (var i = 0; i < searchPanelConfigs.length; i++) {
					var panel = new QGIS.SearchPanel(searchPanelConfigs[i]);
					panel.on("featureselected", showFeatureSelected);
					panel.on("featureselectioncleared", clearFeatureSelected);
					searchTabPanel.add(panel);
				}
				searchTabPanel.setActiveTab(1);
			} else {
				// hide search panel
				var searchPanel = Ext.getCmp('SearchPanel');
				searchPanel.removeAll();
				searchPanel.hide();
			}
		}
}

//"projectpath": "{{ project.get_absolute_path_to_project_filepath }}",
//"projectfile": "{{ project.filename_without_ext }}"

DjangoQGIS.getWidgetOptions = function(url){
	Ext.Ajax.disableCaching = false;
	Ext.Ajax.request({
		url:url,
		success:function(response){
			var data = Ext.util.JSON.decode(response.responseText);
			DjangoQGIS.setProjectWidgetsOptions(data);
		}
	});
	//Ext.Ajax.disableCaching = true;
};

(function(){
	var url = DjangoQGIS.gis_projects_widget_options_url+DjangoQGIS.projectslug;
	DjangoQGIS.reloadingpanels = false;
	DjangoQGIS.getWidgetOptions(url);
})();
