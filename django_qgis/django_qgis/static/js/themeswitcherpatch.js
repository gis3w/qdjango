var themecallback = ThemeSwitcher.prototype.changeTheme;

ThemeSwitcher.prototype.changeTheme = function(dataView, index, node, evt){
	var themeswitcher = this;
	var project_slug =  dataView.getSelectedRecords()[0].data.data.slug;
	var url = DjangoQGIS.gis_projects_widget_options_url+project_slug;
	DjangoQGIS.reloadingpanels = true;
	DjangoQGIS.getWidgetOptions(url);
	themecallback.apply(themeswitcher,[dataView, index, node, evt]);
};