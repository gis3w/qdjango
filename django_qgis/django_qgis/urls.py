import re
from django.conf import settings 
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

def relocate_site(urlpatterns, relocate_settings=('LOGIN_URL',
                                                        'LOGOUT_URL',
                                                        'LOGIN_REDIRECT_URL',
                                                        'MEDIA_URL',
                                                        'ADMIN_MEDIA_PREFIX')):
    '''Relocate a site under a different mount point.

    Typically should be used in the top level ``urls.py``::

        from django.conf import settings
        from django.conf.urls.defaults import *

        urlpatterns = patterns('',
            ...
        )

        # define URL_PREFIX in settings.py
        relocate_site(settings.URL_PREFIX, urlpatterns)

    :param root: The site's mount point, e.g. '/myapp/'.
    :params urlpatterns: The top level url patterns list.
    :param relocate_settings: The setting variables with URL values to be also
        relocated. See also http://code.djangoproject.com/ticket/8906.
    '''
    root = settings.URL_PREFIX.strip('/')
    if root:
        for p in urlpatterns or ():
            p._regex = re.compile(r'^%s/%s' % (re.escape(root),
                                              p.regex.pattern.lstrip('^')))
        for name in relocate_settings or ():
            url = getattr(settings, name)
            if url.startswith('/'):
                setattr(settings, name, '/' + root + url)
                a = 1
                
def relocate_url(url):
    root = settings.URL_PREFIX.strip('/')
    if root:
        if url.startswith('/'):
                variable = url.lstrip('/')
                return '/%s/%s' % (root,variable)
    return url


urlpatterns = patterns(
    '',
    url(r'^$', login_required(TemplateView.as_view(
            template_name='home.html'
            )),
        name='home'),

    url(r'^about/$', login_required(TemplateView.as_view(
            template_name='about.html'
            )),
        name='about'),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # Groups and projects
    url(r'', include('projects.urls')),
    
    
    #url(r'login/', TemplateView.as_view(
    #        template_name='login.html'
    #        ),name='login'),
    #)
    url(r'^login/$', 'django.contrib.auth.views.login',name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout',{'next_page': settings.LOGOUT_NEXT_PAGE},name='logout'),
)

print 'logaout: '+settings.LOGOUT_NEXT_PAGE
# To be used in development, to show user-uploaded files
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    
#relocate_site(urlpatterns,('LOGIN_REDIRECT_URL','LOGIN_URL'))
