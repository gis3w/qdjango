import json

class widget_serializer(object):
    def __init__(self,widget):
        self.widget = widget
    
    def json(self):
        return self._json()
    
    def _json(self):
        pass

class searchwidget_serializer(widget_serializer):
    def __init__(self,widget):
        widget_serializer.__init__(self, widget)
        
    def _json(self):
        return json.loads(self.widget['body'])
    
class tooltipwidget_serializer(widget_serializer):
    def __init__(self,widget):
        widget_serializer.__init__(self, widget)
    
    def _json(self):
        return {'layer':self.widget['layer'].name,'options':json.loads(self.widget['body'])}
    
widget_serialisers = {
    'search': searchwidget_serializer,
    'tooltip': tooltipwidget_serializer
}
    
def get_widget_serializer(type):
    return widget_serialisers[type]
        