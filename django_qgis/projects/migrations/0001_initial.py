# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Group'
        db.create_table(u'projects_group', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('slug', self.gf('autoslug.fields.AutoSlugField')(unique=True, max_length=50, populate_from='name', unique_with=())),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('lang', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('header_logo_img', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('header_logo_height', self.gf('django.db.models.fields.IntegerField')()),
            ('header_logo_link', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('max_scale', self.gf('django.db.models.fields.IntegerField')()),
            ('min_scale', self.gf('django.db.models.fields.IntegerField')()),
            ('panoramic_max_scale', self.gf('django.db.models.fields.IntegerField')()),
            ('panoramic_min_scale', self.gf('django.db.models.fields.IntegerField')()),
            ('units', self.gf('django.db.models.fields.CharField')(default='meters', max_length=255)),
            ('srid', self.gf('django.db.models.fields.IntegerField')()),
            ('use_commercial_maps', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('header_terms_of_use_text', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('header_terms_of_use_link', self.gf('django.db.models.fields.URLField')(max_length=200, blank=True)),
        ))
        db.send_create_signal(u'projects', ['Group'])

        # Adding model 'Project'
        db.create_table(u'projects_project', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('qgis_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('slug', self.gf('autoslug.fields.AutoSlugField')(unique=True, max_length=50, populate_from='title', unique_with=())),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('thumbnail', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['projects.Group'])),
            ('initial_extent', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('max_extent', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('is_panoramic_map', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'projects', ['Project'])

        # Adding unique constraint on 'Project', fields ['title', 'group']
        db.create_unique(u'projects_project', ['title', 'group_id'])

        # Adding model 'Widget'
        db.create_table(u'projects_widget', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('datasource', self.gf('django.db.models.fields.TextField')()),
            ('widget_type', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('autoslug.fields.AutoSlugField')(unique=True, max_length=50, populate_from='name', unique_with=())),
        ))
        db.send_create_signal(u'projects', ['Widget'])

        # Adding model 'Layer'
        db.create_table(u'projects_layer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('slug', self.gf('autoslug.fields.AutoSlugField')(unique=True, max_length=50, populate_from='name', unique_with=())),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('project', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['projects.Project'])),
            ('layer_type', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('datasource', self.gf('django.db.models.fields.TextField')()),
            ('is_visible', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('order', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('data_file', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('database_columns', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'projects', ['Layer'])

        # Adding unique constraint on 'Layer', fields ['name', 'project']
        db.create_unique(u'projects_layer', ['name', 'project_id'])

        # Adding model 'LayerWidget'
        db.create_table(u'projects_layerwidget', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('widget', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['projects.Widget'])),
            ('layer', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['projects.Layer'])),
            ('alias', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal(u'projects', ['LayerWidget'])


    def backwards(self, orm):
        # Removing unique constraint on 'Layer', fields ['name', 'project']
        db.delete_unique(u'projects_layer', ['name', 'project_id'])

        # Removing unique constraint on 'Project', fields ['title', 'group']
        db.delete_unique(u'projects_project', ['title', 'group_id'])

        # Deleting model 'Group'
        db.delete_table(u'projects_group')

        # Deleting model 'Project'
        db.delete_table(u'projects_project')

        # Deleting model 'Widget'
        db.delete_table(u'projects_widget')

        # Deleting model 'Layer'
        db.delete_table(u'projects_layer')

        # Deleting model 'LayerWidget'
        db.delete_table(u'projects_layerwidget')


    models = {
        u'projects.group': {
            'Meta': {'object_name': 'Group'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'header_logo_height': ('django.db.models.fields.IntegerField', [], {}),
            'header_logo_img': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'header_logo_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'header_terms_of_use_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'header_terms_of_use_text': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'max_scale': ('django.db.models.fields.IntegerField', [], {}),
            'min_scale': ('django.db.models.fields.IntegerField', [], {}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'panoramic_max_scale': ('django.db.models.fields.IntegerField', [], {}),
            'panoramic_min_scale': ('django.db.models.fields.IntegerField', [], {}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique': 'True', 'max_length': '50', 'populate_from': "'name'", 'unique_with': '()'}),
            'srid': ('django.db.models.fields.IntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'units': ('django.db.models.fields.CharField', [], {'default': "'meters'", 'max_length': '255'}),
            'use_commercial_maps': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'projects.layer': {
            'Meta': {'ordering': "['order']", 'unique_together': "(('name', 'project'),)", 'object_name': 'Layer'},
            'data_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'database_columns': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'datasource': ('django.db.models.fields.TextField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_visible': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'layer_type': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'order': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'project': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Project']"}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique': 'True', 'max_length': '50', 'populate_from': "'name'", 'unique_with': '()'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'widgets': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['projects.Widget']", 'null': 'True', 'through': u"orm['projects.LayerWidget']", 'blank': 'True'})
        },
        u'projects.layerwidget': {
            'Meta': {'object_name': 'LayerWidget'},
            'alias': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'layer': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Layer']"}),
            'widget': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Widget']"})
        },
        u'projects.project': {
            'Meta': {'unique_together': "(('title', 'group'),)", 'object_name': 'Project'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'initial_extent': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_panoramic_map': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'max_extent': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'qgis_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique': 'True', 'max_length': '50', 'populate_from': "'title'", 'unique_with': '()'}),
            'thumbnail': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'projects.widget': {
            'Meta': {'object_name': 'Widget'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'datasource': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique': 'True', 'max_length': '50', 'populate_from': "'name'", 'unique_with': '()'}),
            'widget_type': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['projects']