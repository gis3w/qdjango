DjangoQGIS.gis_projects_widget_options_url = "{% url 'gis-project-widget-options' '' %}"
DjangoQGIS.mediaUrl = "{{ MEDIA_URL }}"

var gis_projects = {
    "path": "/site",
    "mapserver": "/cgi-bin/qgis_mapserv.fcgi",
    "thumbnails": "/thumbnails",
    "title": "{{ object.name }}",
    "topics": [
	{% for project in object.project_set.all %}
	{
	    "name": "{{ project.name }}",
	    "projects": [{
		"name": "{{ project.title }}",
		"projectpath": "{{ project.get_absolute_path_to_project_filepath }}",
		"projectfile": "{{ project.filename_without_ext }}",
		"thumbnail": "{{ project.thumbnail }}",
		"slug": "{{ project.slug }}", {% comment %} To retrieve project slug for theme switch (not present in QgisWebClient original options) {% endcomment %}
		"format": "image/png",
		"visibleLayers": "{% for layer in project.get_visible_layers %}{{ layer.name }}{% if not forloop.last %},{% endif %}{% endfor %}",
		"updateInterval": "occasional"{% comment %},
		"responsible": "",
		"tags": ""
		{% endcomment %}
	    }]
	}{% if not forloop.last %}, {% endif %}{% endfor %}
    ]
};