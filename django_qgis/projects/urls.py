import os

from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
from django.views.generic import RedirectView

from .views import (
    GroupListView,
    GroupCreateView,
    GroupDetailView,
    GroupUpdateView,
    GroupDeleteView,
    GroupGlobalOptionsView,
    GroupGISProjectListingView,
    WidgetOptionsView,
    ProjectCreateView,
    ProjectDetailView,
    ProjectUpdateView,
    ProjectDeleteView,
    ProjectShowMapView,
    LayerDetailView,
    LayerUpdateView,
    LayerDeleteView,
    WidgetListView,
    WidgetCreateView,
    WidgetDetailView,
    WidgetUpdateView,
    WidgetDeleteView,
    #WidgetLinkView,
    #WidgetUnlinkView,
    QGISRedirectView,
    )


urlpatterns = patterns(
    '',
    # Groups
    url(r'^groups/$', login_required(GroupListView.as_view()), name='group-list'),
    url(r'^groups/add/$', login_required(GroupCreateView.as_view()), name='group-create'),
    url(
        r'^groups/(?P<slug>[-_\w\d]+)/$',
        login_required(GroupDetailView.as_view()),
        name='group-detail'
        ),
    url(
        r'^groups/(?P<slug>[-_\w\d]+)/update/$',
        login_required(GroupUpdateView.as_view()),
        name='group-update'
        ),
    url(
        r'^groups/(?P<slug>[-_\w\d]+)/delete/$',
        login_required(GroupDeleteView.as_view()),
        name='group-delete'
        ),
    url(
        r'^groups/(?P<slug>[-_\w\d]+)/add-project/$',
        login_required(ProjectCreateView.as_view()),
        name='project-create'
        ),
    url(
        r'^groups/(?P<slug>[-_\w\d]+)/GlobalOptions.js$',
        GroupGlobalOptionsView.as_view(),
        name='group-global-options'
        ),
    url(
        r'^groups/(?P<slug>[-_\w\d]+)/GISProjectListing.js$',
        GroupGISProjectListingView.as_view(),
        name='group-gis-project-listing'
        ),
    url(
        r'^projects/widgetoptions/(?P<slug>[-_\w\d]*)$',
        WidgetOptionsView.as_view(),
        name='gis-project-widget-options'
        ),
    # Projects
    url(
        r'^projects/(?P<slug>[-_\w\d]+)/$',
        login_required(ProjectDetailView.as_view()),
        name='project-detail'
        ),
    url(
        r'^projects/(?P<slug>[-_\w\d]+)/update/$',
        login_required(ProjectUpdateView.as_view()),
        name='project-update'
        ),
    url(
        r'^projects/(?P<slug>[-_\w\d]+)/delete/$',
        login_required(ProjectDeleteView.as_view()),
        name='project-delete'
        ),
    url(
        r'^projects/(?P<slug>[-_\w\d]+)/show-map/$',
        ProjectShowMapView.as_view(),
        name='project-show-map'
        ),
    # WARNING: temporary ugly hack, fix it!
    url(
        r'^projects/(?P<slug>[-_\w\d]+)/show-map/(?P<location>.+)',
        QGISRedirectView.as_view()
        ),
    # Layers
    url(
        r'^layers/(?P<slug>[-_\w\d]+)/$',
        login_required(LayerDetailView.as_view()),
        name='layer-detail'
        ),
    url(
        r'^layers/(?P<slug>[-_\w\d]+)/update/$',
        login_required(LayerUpdateView.as_view()),
        name='layer-update'
        ),
    url(
        r'^layers/(?P<slug>[-_\w\d]+)/delete/$',
        login_required(LayerDeleteView.as_view()),
        name='layer-delete'
        ),
    # Widgets
    url(
        r'^widgets/$',
        login_required(WidgetListView.as_view()),
        name='widget-list'
        ),
    url(
        r'^layers/(?P<slug>[-_\w\d]+)/add-widget/$',
        login_required(WidgetCreateView.as_view()),
        name='widget-create'
        ),
    url(
        r'^widgets/(?P<slug>[-_\w\d]+)/$',
        login_required(WidgetDetailView.as_view()),
        name='widget-detail'
        ),
    url(
        r'^widgets/(?P<slug>[-_\w\d]+)/update/$',
        login_required(WidgetUpdateView.as_view()),
        name='widget-update'
        ),
    url(
        r'^widgets/(?P<slug>[-_\w\d]+)/delete/$',
        login_required(WidgetDeleteView.as_view()),
        name='widget-delete'
        ),
    url(
        r'^layers/(?P<slug>[-_\w\d]+)/link/(?P<widgetslug>[-_\w\d]+)$',
        login_required(LayerDetailView.as_view()),
        name='widget-link'
        ),
    url(
        r'^layers/(?P<slug>[-_\w\d]+)/unlink/(?P<widgetslug>[-_\w\d]+)$',
        login_required(LayerDetailView.as_view()),
        name='widget-unlink'
        ),
    )
