import os, json

from django.conf import settings
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response,render, redirect, get_object_or_404
from django.utils.translation import ugettext, ugettext_lazy as _
from django.db import IntegrityError
from django.views.generic import (
    ListView,
    DeleteView,
    DetailView,
    UpdateView,
    FormView,
    CreateView,
    RedirectView
    )

from .models import Layer, Project, Group, Widget, LayerWidget
from .forms import (
    GroupForm,
    ProjectCreateForm,
    ProjectUpdateForm,
    LayerUpdateForm,
    ExtendedLayerUpdateForm,
    WidgetCreateForm,
    WidgetUpdateForm,
    #WidgetLinkForm,
    )
from .utils.project_import import create_qgis_project, update_qgis_project, is_group_compatible
from .serializers import get_widget_serializer
#from django_qgis.exceptions import render_error


class GroupListView(ListView):
    """List view."""

    model = Group


class GroupCreateView(CreateView):
    """Create view."""

    model = Group
    form_class = GroupForm
    success_url = reverse_lazy('group-list')


class GroupDetailView(DetailView):
    """Detail view."""

    model = Group

    def get_context_data(self, **kwargs):
        """Add project absolute path (ie: MEDIA_ROOT) to context."""

        context = super(GroupDetailView, self).get_context_data(**kwargs)
        context['MEDIA_ROOT'] = settings.MEDIA_ROOT
        return context

    def post(self, request, *args, **kwargs):
        """Set given project as map for current group."""

        slug = request.POST.get('set-project-as-group-map')
        try:
            project = Project.objects.get(slug=slug)
        except Project.DoesNotExist:
            pass
        else:
            project.is_panoramic_map = True
            project.save()

        group = self.get_object()
        return HttpResponseRedirect(group.get_absolute_url())


class GroupUpdateView(UpdateView):
    """Update view."""

    model = Group
    form_class = GroupForm


class GroupDeleteView(DeleteView):
    """Delete view."""

    model = Group
    success_url = reverse_lazy('group-list')

class ProjectCreateView(FormView):
    """Create view."""

    form_class = ProjectCreateForm
    template_name = 'projects/project_form.html'
    group = None
    
    def get_form_kwargs(self):
        kwargs = super(ProjectCreateView,self).get_form_kwargs()
        kwargs['request'] = self.request
        kwargs['group'] = self.group
        return kwargs

    def dispatch(self, request, *args, **kwargs):
        """Populate group attribute."""

        self.group = get_object_or_404(Group, slug=self.kwargs['slug'])
        return super(ProjectCreateView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """Add current group to context."""

        context = super(ProjectCreateView, self).get_context_data(**kwargs)
        context['group'] = self.group
        return context
    
    def render_to_response(self, context, **response_kwargs):
        dummy = self.request.user.is_authenticated()
        return super(ProjectCreateView, self).render_to_response(context, **response_kwargs)
        

    def form_valid(self, form):
        """Form and logic behind project creation."""

        qgis_file = form.cleaned_data['qgis_file']
        thumbnail = form.cleaned_data['thumbnail']
        description = form.cleaned_data['description']
        qgis_data = form.qgis_project_data
        
        try:
            project = create_qgis_project(
                qgis_file=qgis_file,
                qgis_data=qgis_data,
                group=self.group,
                thumbnail=thumbnail,
                description=description,
                )
        except Exception as e:
            return render(self.request,'generic_error.html',{'error':{'msg':unicode(e)}})
        
        return HttpResponseRedirect(self.group.get_absolute_url())  


class ProjectUpdateView(UpdateView):
    """Update view."""

    model = Project
    form_class = ProjectUpdateForm

    def form_valid(self, form):
        """Form and logic behind project update."""

        project = form.instance
        qgis_file = form.cleaned_data['qgis_file']
        thumbnail = form.cleaned_data['thumbnail']
        description = form.cleaned_data['description']

        try:
            project = update_qgis_project(
                qgis_file=qgis_file,
                project=project,
                thumbnail=thumbnail,
                description=description,
                )
        except Exception as e:
            return render(self.request,'generic_error.html',{'error':{'msg':unicode(e)}})
        
        return HttpResponseRedirect(project.get_absolute_url())


class ProjectDetailView(DetailView):
    """Detail view."""

    model = Project

    def get_context_data(self, **kwargs):
        """Add project absolute path (ie: MEDIA_ROOT) to context."""

        context = super(ProjectDetailView, self).get_context_data(**kwargs)
        context['MEDIA_ROOT'] = settings.MEDIA_ROOT
        return context

    def post(self, request, *args, **kwargs):
        """Set current layer visibility."""

        slug_list = request.POST.getlist('set-layer-visibility')
        project = self.get_object()
        for layer in project.layer_set.all():
            if layer.slug in slug_list:
                layer.is_visible = True
            else:
                layer.is_visible = False
            layer.save()

        return HttpResponseRedirect(project.get_absolute_url())


class ProjectDeleteView(DeleteView):
    """Delete view."""

    model = Project

    def post(self, *args, **kwargs):
        self.group = self.get_object().group
        return super(ProjectDeleteView, self).post(*args, **kwargs)

    def get_success_url(self):
        return reverse('group-detail', kwargs={'slug': self.group.slug})


class LayerDetailView(DetailView):
    """Detail view."""

    model = Layer
    
    def dispatch(self, request, *args, **kwargs):
        self.layer = get_object_or_404(Layer, slug=self.kwargs['slug'])
        return super(LayerDetailView, self).dispatch(request, *args, **kwargs)

    def get(self, *args, **kwargs):
        request_url_name = self.request.resolver_match.url_name
        if request_url_name == 'widget-link':
            self.link_unlink_widget()
        elif request_url_name == 'widget-unlink':
            self.link_unlink_widget(False)
            
        return super(LayerDetailView, self).get(*args, **kwargs)
    
    def link_unlink_widget(self,link=True):
        self.widget = get_object_or_404(Widget, slug=self.kwargs['widgetslug'])
        if self.layer.datasource != self.widget.datasource:
            raise Http404
        layer_widget, created = LayerWidget.objects.get_or_create(
            layer=self.layer,
            widget=self.widget,
            )
        if link:   
            layer_widget.save()
        else:
            layer_widget.delete()

    def get_context_data(self, **kwargs):
        context = super(LayerDetailView, self).get_context_data(**kwargs)
        current_object = self.get_object()
        widgets = Widget.objects.filter(
            datasource=current_object.datasource
            ).values()
        temp = []
        for current_widget in widgets:
            try:
                current_widget['layerwidget'] = LayerWidget.objects.get(
                    layer=current_object, widget__pk=current_widget['id']
                    )
            except LayerWidget.DoesNotExist:
                current_widget['layerwidget'] = None
            temp.append(current_widget)
        context['widgets'] = temp
        return context


class LayerUpdateView(UpdateView):
    """Update view."""

    model = Layer

    def get_form_class(self):
        layer = self.get_object()
        if layer.layer_type == Layer.TYPES.postgres:
            return LayerUpdateForm
        return ExtendedLayerUpdateForm


class LayerDeleteView(DeleteView):
    """Delete view."""

    model = Layer

    def post(self, *args, **kwargs):
        self.project = self.get_object().project
        return super(LayerDeleteView, self).post(*args, **kwargs)

    def get_success_url(self):
        return reverse('project-detail', kwargs={'slug': self.project.slug})



class WidgetListView(ListView):
    """List view."""

    model = Widget


class WidgetCreateView(CreateView):
    """Create view."""

    model = Widget
    form_class = WidgetCreateForm

    def dispatch(self, request, *args, **kwargs):
        self.layer = get_object_or_404(Layer, slug=self.kwargs['slug'])
        return super(WidgetCreateView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(WidgetCreateView, self).get_context_data(**kwargs)
        context['layer'] = self.layer
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.datasource = self.layer.datasource
        return super(WidgetCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('layer-detail', kwargs={'slug': self.layer.slug})


class WidgetDetailView(DetailView):
    """Detail view."""

    model = Widget


class WidgetUpdateView(UpdateView):
    """Update view."""

    model = Widget
    form_class = WidgetUpdateForm

    def get_context_data(self, **kwargs):
        layer = Layer.objects.filter(datasource=self.object.datasource)[0]
        context = super(WidgetUpdateView, self).get_context_data(**kwargs)
        context['layer'] = layer        
        return context
    
    def get_success_url(self):
        return reverse('widget-list')


class WidgetDeleteView(DeleteView):
    """Delete view."""

    model = Widget

    def get_success_url(self):
        return reverse('widget-list')


class GroupGlobalOptionsView(DetailView):
    """Serve ``GlobalOptions.js`` file for a specific group."""

    model = Group
    template_name = 'projects/group_globaloptions.js'

    def get_context_data(self, **kwargs):
        """Add map project to context."""

        group = self.get_object()
        context = super(GroupGlobalOptionsView, self).get_context_data(**kwargs)
        try:
            map_project = Project.objects.get(
                group=group, is_panoramic_map=True
                )
        except Project.DoesNotExist:
            pass
        else:
            context['map_project'] = map_project
        return context

    def render_to_response(self, context, **kwargs):
        """This is a js file: properly set mimetype."""

        return super(GroupGlobalOptionsView, self).render_to_response(
            context, content_type='text/javascript', **kwargs
            )


class GroupGISProjectListingView(DetailView):
    """Serve ``GISProjectListing.js`` file for a specific group."""

    model = Group
    template_name = 'projects/group_gisprojectlisting.js'

    def render_to_response(self, context, **kwargs):
        """This is a js file: properly set mimetype."""

        return super(GroupGISProjectListingView, self).render_to_response(
            context, content_type='text/javascript', **kwargs
            )
        
class WidgetOptionsView(DetailView):
    model = Project
    #template_name = 'projects/group_widgetoptions.js'
    _widgets_dict = dict((t[0],[]) for t in Widget.TYPES)     
    
    def render_to_response(self, context, **kwargs):
        widgets_options = {'wmsMapName':'','widgets':None}
        widgets_dict = dict((t[0],[]) for t in Widget.TYPES)
        
        wmsMapName = os.path.join(self.object.get_absolute_path_to_project_filepath(),self.object.filename_without_ext());
        layers = self.object.layer_set.all()
        for layer_object in layers:
            widgets = Widget.objects.filter(
            datasource=layer_object.datasource
            ).values()
            for current_widget in widgets:
                current_widget['layer'] = layer_object
                widget = {}
                if LayerWidget.objects.filter(
                        layer=layer_object, widget__pk=current_widget['id']
                        ).exists():
                    if current_widget['body']!= '':
                        try:
                            serializer = get_widget_serializer(current_widget['widget_type'])
                            widget_json = serializer(current_widget).json()
                            widgets_dict[current_widget['widget_type']].append(widget_json)
                        except Exception as e:
                            print e
        widgets_options['wmsMapName'] = wmsMapName
        widgets_options['widgets'] = widgets_dict
        return HttpResponse(json.dumps(widgets_options), content_type="application/json")



class ProjectShowMapView(DetailView):
    """Serve the project QGIS web client page."""

    model = Project
    template_name = 'projects/project_qgiswebclient.html'


class QGISRedirectView(RedirectView):
    """Redirect QGIS to the correct static location.

    QGIS looks for some of its file under a wrong relative
    path. Redirect to the correct location.

    WARNING: this is a temporary ugly hack. Fix it!
    """

    def get_redirect_url(self, slug, location):
        return os.path.join(settings.STATIC_URL, 'qgiswebclient', location)
