import os

from autoslug import AutoSlugField
from django.conf import settings
from django.conf.global_settings import LANGUAGES
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext, ugettext_lazy as _
from model_utils import Choices
from model_utils.models import TimeStampedModel

from projects.utils.storage import OverwriteStorage
from projects.utils.database import get_database_columns


class Group(TimeStampedModel):
    """A group of projects."""
    UNITS = Choices(
        ('meters', _('Meters')),
        ('feet', _('Feet')),
        ('degrees', _('Degrees')),
        )
    # General info
    name = models.CharField(_('Name'), max_length=255, unique=True)
    title = models.CharField(_('Title'), max_length=255)
    description = models.TextField(_('Description'), blank=True)
    slug = AutoSlugField(
        _('Slug'), populate_from='name', unique=True, always_update=True
        )
    is_active = models.BooleanField(_('Is active'), default=1)
    # l10n
    lang = models.CharField(_('lang'), max_length=20, choices=LANGUAGES)
    # Company logo
    header_logo_img = models.FileField(_('headerLogoImg'), upload_to='logo_img')
    header_logo_height = models.IntegerField(_('headerLogoHeight'))
    header_logo_link = models.URLField(_('headerLogoLink'), blank=True, null=True)
    # Max/min scale
    max_scale = models.IntegerField(_('Max scale'))
    min_scale = models.IntegerField(_('Min scale'))
    # Panoramic max/min scale
    panoramic_max_scale = models.IntegerField(_('Panoramic max scale'))
    panoramic_min_scale = models.IntegerField(_('Panoramic min scale'))
    # Unit of measure
    #units = models.CharField(_('Units'), max_length=255)
    units = models.CharField(_('Units'), choices=UNITS, max_length=255, default=UNITS[0][0])
    # Group SRID (a.k.a. EPSG)
    srid = models.IntegerField(_('SRID/EPSG'))
    # use Google/Bing base maps
    use_commercial_maps = models.BooleanField(_('Use commercial maps'))
    # Company TOS
    header_terms_of_use_text = models.TextField(
        _('headerTermsOfUseText'), blank=True
        )
    header_terms_of_use_link = models.URLField(
        _('headerTermsOfUseLink'), blank=True
        )

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('group-detail', kwargs={'slug': self.slug})


class Project(TimeStampedModel):
    """A QGIS project."""

    def get_project_file_path(instance, filename):
        """Custom name for uploaded project files."""

        group_name = slugify(unicode(instance.group.name))
        project_name = slugify(unicode(instance.title))
        filename = u'%s_%s.qgs' % (group_name, project_name)
        return os.path.join('projects', filename)

    def get_thumbnail_path(instance, filename):
        """Custom name for uploaded thumbnails."""

        group_name = slugify(unicode(instance.group.name))
        project_name = slugify(unicode(instance.title))
        ext = filename.split('.')[-1]
        filename = u'%s_%s.%s' % (group_name, project_name, ext)
        return os.path.join('thumbnails', filename)

    # Project file
    qgis_file = models.FileField(
        _('QGIS project file'),
        upload_to=get_project_file_path,
        storage=OverwriteStorage()
        )

    # General info
    #name = models.CharField(_('Name'), max_length=255)
    title = models.CharField(_('Title'), max_length=255)
    description = models.TextField(_('Description'), blank=True)
    slug = AutoSlugField(
        _('Slug'), populate_from='title', unique=True, always_update=True
        )
    is_active = models.BooleanField(_('Is active'), default=1)

    # Thumbnail
    thumbnail = models.ImageField(
        _('Thumbnail'),
        upload_to=get_thumbnail_path,
        storage=OverwriteStorage(),
        blank=True,
        null=True
        )

    # Group
    group = models.ForeignKey(Group, verbose_name=_('Group'))
    
    # Extent
    initial_extent = models.CharField(_('Initial extent'), max_length=255)
    max_extent = models.CharField(_('Max extent'), max_length=255)

    # Whether current project acts as a panoramic map within group
    is_panoramic_map = models.BooleanField(_('Is panoramic map'), default=0)

    class Meta:
        unique_together = (('title', 'group'))

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('project-detail', kwargs={'slug': self.slug})

    def get_absolute_path(self):
        """Get absolute filesystem path of project file.

        WARNING: we are using this inside a template
        (``GlobalOptions.js``), so that filesystem path is publicly
        revealed. That can be a security breach. Fix it!
        """

        return os.path.join(settings.MEDIA_ROOT, self.qgis_file.name)
		
    def get_absolute_path_to_project_filepath(self):
        return os.path.join(settings.MEDIA_ROOT,self.filepath())

    def get_initial_extent_list(self):
        """Get initial extent string as a list of values."""

        return self.initial_extent.split(';')

    def filepath(self):
        """Get path of related file.

        Eg: ``projects/`` instead of ``projects/helloworld.qgs``
        """
        
        return os.path.dirname(self.qgis_file.name)

    def filename(self):
        """Get related filename without path.

        Eg: ``helloworld.qgs`` instead of ``projects/helloworld.qgs``
        """
        
        return os.path.basename(self.qgis_file.name)

    def filename_without_ext(self):
        """Get related filename without path and without extension.

        Eg: ``helloworld`` instead of ``projects/helloworld.qgs``
        """

        return os.path.splitext(self.filename())[0]

    def thumbname(self):
        return os.path.basename(self.thumbnail.name)

    def get_visible_layers(self):
        return self.layer_set.filter(is_visible=True)

    def save(self, *args, **kwargs):
        """Custom save method."""

        # Set a default max_extent
        if not self.max_extent:
            self.max_extent = self.initial_extent
        # At most one "map" project per group
        if self.is_panoramic_map:
            try:
                temp = Project.objects.get(
                    is_panoramic_map=True, group=self.group
                    )
            except Project.DoesNotExist:
                pass
            else:
                if temp != self:
                    temp.is_panoramic_map = False
                    temp.save()
        super(Project, self).save(*args, **kwargs)


class Widget(models.Model):
    """A search widget."""

    TYPES = Choices(
        #('hyperlink', _('Hyperlink')),
        ('search', _('Search')),
        ('tooltip', _('Tooltip')),
        )
    name = models.CharField(_('Name'), max_length=255)
    body = models.TextField(_('Body'))
    datasource = models.TextField(_('datasource'))
    widget_type = models.CharField(_('Type'), choices=TYPES, max_length=255)
    slug = AutoSlugField(
        _('Slug'), populate_from='name', unique=True, always_update=True
        )

    def __unicode__(self):
        return self.name


class Layer(models.Model):
    """A QGIS layer."""

    TYPES = Choices(
        ('postgres', _('Postgres')),
        ('spatialite', _('SpatiaLite')),
        ('wfs', _('WFS')),
        ('wms', _('WMS')),
        ('ogr', _('OGR')),
        ('gdal', _('GDAL')),
        )
    # General info
    name = models.CharField(_('Name'), max_length=255)
    title = models.CharField(_('Title'), max_length=255, blank=True)
    description = models.TextField(_('Description'), blank=True)
    slug = AutoSlugField(
        _('Slug'), populate_from='name', unique=True, always_update=True
        )
    is_active = models.BooleanField(_('Is active'), default=1)
    # Project
    project = models.ForeignKey(Project, verbose_name=_('Project'))
    # Type and content
    layer_type = models.CharField(_('Type'), choices=TYPES, max_length=255)
    datasource = models.TextField(_('Datasource'))
    is_visible = models.BooleanField(_('Is visible'), default=1)
    order = models.IntegerField(_('Order'),default=1)
    # Optional data file (non-postgres layers need it)
    data_file = models.FileField(
        _('Associated data file'),
        upload_to='data-files',
        blank=True,
        null=True
        )
    # Database columns (postgres layers need it)
    database_columns = models.TextField(_('Database columns'), blank=True)
    # Widgets
    widgets = models.ManyToManyField(
        Widget,
        verbose_name=_('Widgets'),
        through='LayerWidget',
        blank=True,
        null=True
        )

    def __unicode__(self):
        return self.name

    class Meta:
        unique_together = (('name', 'project',),)
        ordering = ['order']

    def get_absolute_url(self):
        return reverse('layer-detail', kwargs={'slug': self.slug})

    def needs_data_file(self):
        """Whether a data file is needed for current layer."""

        if self.layer_type != self.TYPES.postgres and not self.data_file:
            return True

    def set_database_columns(self):
        """Get database columns and set the relative layer field."""

        if self.layer_type == self.TYPES.postgres:
            self.database_columns = get_database_columns(self.datasource)
            self.save()


class LayerWidget(models.Model):
    """A widget for a layer."""

    widget = models.ForeignKey(Widget, verbose_name=_('Widget'))
    layer = models.ForeignKey(Layer, verbose_name=_('Layer'))
    #alias = models.CharField(_('Alias'), max_length=255, blank=True)

    def __unicode__(self):
        return "%s - %s" % (self.widget.name,self.layer.name)
    
