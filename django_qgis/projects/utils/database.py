import json

from sqlalchemy import create_engine, Table
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.schema import MetaData
import psycopg2


def get_database_columns(datasource):
    """Parse datasource string and introspect database."""
    
    # Parse datasource string
    datadict = {}
    datalist = datasource.split(' ')
    for item in datalist:
        try:
            key, value = item.split('=')
            datadict[key] = value.strip('\'')
        except ValueError:
            pass

    # Connection details
    driver = 'postgresql+psycopg2'
    username = datadict['user']
    password = datadict['password']
    host = datadict['host']
    port = datadict['port']
    database = datadict['dbname']
    schema, table = get_schema_table(datadict['table'])
    schema = schema.strip('"')
    table = table.strip('"')

    # Connection url
    url = URL(driver, username, password, host, port, database)
    
    # Some SQLAlchemy magic
    Base = declarative_base()
    engine = create_engine(url, echo=False)
    Session = sessionmaker(bind=engine)
    session = Session()
    meta = MetaData(bind=engine)
    messages = Table(
        table, meta, autoload=True, autoload_with=engine, schema=schema
        )

    database_columns = []
    for c in messages.columns:
        try:
            database_columns.append({'name': c.name, 'type': str(c.type)})
        except NotImplementedError:
            pass

    #return json.dumps(database_columns)
    return database_columns

def get_schema_table(field):
    if field.find('.')!=-1:
        schema, table = field.split('.')
    else:
        schema = 'public'
        table = field
    return schema, table

# Eg:
# >>> datasource = '''dbname='regione_toscana' host=django-qgis.fnstudio.it port=5432 user='master' password='master' sslmode=disable key='gid' srid=3003 type=MULTIPOLYGON table="dati_base"."riserve_statali" (the_geom) sql='''
# >>> print get_database_columns(datasource)
# [{"type": "INTEGER", "name": "gid"}, {"type": "DOUBLE PRECISION", "name": "area"}, {"type": "DOUBLE PRECISION", "name": "perimeter"}, {"type": "DOUBLE PRECISION", "name": "ap_rns_"}, {"type": "DOUBLE PRECISION", "name": "ap_rns_id"}, {"type": "VARCHAR(6)", "name": "codice"}, {"type": "VARCHAR(1)", "name": "geometria"}, {"type": "VARCHAR(100)", "name": "nome"}, {"type": "VARCHAR(3)", "name": "codprov"}, {"type": "VARCHAR(8)", "name": "min_cod"}]
