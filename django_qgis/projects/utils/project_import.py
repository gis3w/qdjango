import os
import re
import json

from defusedxml import lxml
from django.conf import settings
from django.db import transaction
from django.utils.translation import ugettext, ugettext_lazy as _

from projects.utils import database
from projects.models import Layer, Project


def get_xml_tree(qgis_file):
    """Parse the XML file."""

    try:
        qgis_file.file.seek(0) # we have to rewind the underlying file in case it has been already parsed
        tree = lxml.parse(qgis_file,forbid_entities=False)
    except Exception as e:
        raise Exception(_('The project file is malformed'))
    return tree


def get_project_name(tree):
    """Get project name."""

    try:
        name = tree.getroot().attrib['projectname']
    except:
        name = None
    if not name:
        raise Exception(_('Missing or invalid project name'))
    return name


def get_project_title(tree):
    """Get project title."""

    try:
        title = tree.find('title').text
    except:
        title = None
    if not title:
        raise Exception(_('Missing or invalid project title'))
    return title


def get_project_initial_extent(tree):
    """Get project initial extent."""

    try:
        initial_extent_xmin = tree.find('mapcanvas/extent/xmin').text
        initial_extent_ymin = tree.find('mapcanvas/extent/ymin').text
        initial_extent_xmax = tree.find('mapcanvas/extent/xmax').text
        initial_extent_ymax = tree.find('mapcanvas/extent/ymax').text
    except:
        initial_extent_xmin = None
        initial_extent_ymin = None
        initial_extent_xmax = None
        initial_extent_ymax = None
    if not (
        initial_extent_xmin and
        initial_extent_ymin and
        initial_extent_xmax and
        initial_extent_ymax
        ):
        raise Exception(_('Missing or invalid project initial extent'))
    return '%s;%s;%s;%s' % (
        initial_extent_xmin,
        initial_extent_ymin,
        initial_extent_xmax,
        initial_extent_ymax,
        )


def get_project_srid(tree):
    """Get project SRID."""
    try:
        srid = int(tree.find('mapcanvas/destinationsrs/spatialrefsys/srid').text)
    except:
        srid = None
    if not srid:
        raise Exception(_('Missing or invalid project SRID'))
    return srid


def get_project_units(tree):
    """Get project unit of measure."""

    try:
        units = tree.find('mapcanvas/units').text
    except:
        units = None
    if not units:
        raise Exception(_('Missing or invalid project units'))
    return units


def get_layer_name(tree,order):
    """Get layer name."""

    try:
        layer_name = tree.find('layername').text
    except:
        layer_name = None
    if not layer_name:
        raise Exception(_('Missing or invalid name for layer n.')+' %s' % order)
    return layer_name


def get_layer_title(tree):
    """Get layer title."""

    try:
        layer_title = tree.find('title').text
    except:
        layer_title = ''
    if not layer_title:
        layer_title = ''

    return layer_title


def get_layer_type(tree,name):
    """Get layer type."""

    available_types = [item[0] for item in Layer.TYPES]
    try:
        layer_type = tree.find('provider').text
    except: # WARNING: add a specific value here
        layer_type = None
    if not layer_type in available_types:
        raise Exception(_('Missing or invalid type for layer')+' "%s"' % name)
    return layer_type


def get_layer_datasource(tree,name):
    """Get layer datasource."""

    try:
        layer_datasource = tree.find('datasource').text
    except:
        layer_datasource = None
    if not layer_datasource:
        raise Exception(_('Missing or invalid datasource for layer')+' "%s"' % name)
    return layer_datasource


def get_layer_id(tree,name):
    """Get layer id."""

    try:
        layer_id = tree.find('id').text
    except:
        layer_id = None
    if not layer_id:
        raise Exception(_('Missing or invalid layer id for layer')+' "%s"' % name)
    return layer_id


def get_layer_visibility(tree, layer_id,name):
    """Get layer visibility."""

    legends = tree.iterdescendants(tag='legendlayerfile')

    for legend in legends:
        if legend.attrib['layerid'] == layer_id:
            if legend.attrib['visible'] == '1':
                return True
            else:
                return False

    raise Exception(_('Missing or invalid legend for layer')+' "%s"' % name)

def get_columns_for_joined_layer(tree):
    datasource = get_layer_datasource(tree)
    columns = database.get_database_columns(datasource)
    return columns

def get_layer_joined_columns(tree):
    joined_columns = []
    try:
        joins = tree.find('vectorjoins')
        for join in joins:
            join_id = join.attrib['joinLayerId']
            joined_layer = tree.getroottree().xpath('//id[text()="'+join_id+'"]/..')[0] # prendo l'elemento parent di un tag "id" dove il testo corrisponde al nome del layer
            joined_columns += get_columns_for_joined_layer(joined_layer)
    except Exception,e:
        pass
    return joined_columns

def mangle_layer_columns_with_alias(columns,tree):
    aliased_columns = []
    aliases = tree.findall('aliases/alias')
    for column in columns:
        col_name = column['name']
        for alias in aliases:
            col_alias = alias.attrib['field']
            if col_name == col_alias and  alias.attrib['name']!='':
                column['name'] = alias.attrib['name']
        aliased_columns.append(column)
    return aliased_columns

def filter_compatible_layertype(layer_tree):
    if 'name' in layer_tree.attrib:
        if layer_tree.attrib['name'] == 'openlayers':
            return False
    if 'embedded' in layer_tree.attrib:
        if layer_tree.attrib['embedded'] == '1':
            return False
    return True

def get_project_layers(tree):
    """Get project layers."""

    # Initialize an empty list of layers
    layers = []

    # Get legend tree
    legend_tree = tree.find('legend')

    # Get layer trees
    #layer_trees = tree.findall('projectlayers/maplayer')
    layer_trees = tree.xpath('projectlayers/maplayer[@geometry!="No geometry" or @type="raster"]')

    # Run through the layer trees
    for order,layer_tree in enumerate(layer_trees):
        if filter_compatible_layertype(layer_tree):
            
        # Get layer details
            name = get_layer_name(layer_tree,order)
            layer_id = get_layer_id(layer_tree,name)
            is_visible = get_layer_visibility(legend_tree, layer_id,name)
            title = get_layer_title(layer_tree)
            layer_type = get_layer_type(layer_tree,name)
            datasource = get_layer_datasource(layer_tree,name)
            
            #Test datasource existence
            server_datasource = make_datasource(datasource,layer_type)
            if server_datasource is not None:
                # This strips OGR data access definitions, as for DXF "test.dxf|layername=entities|geometrytype=LineString"
                server_datasource = server_datasource.split('|')[0]
                if not os.path.exists(server_datasource):
                    err = _('Missing data file for layer')+' "%s"' % name
                    err += _('which should be located at')+' "%s"' % server_datasource
                    raise Exception(err)
            
            columns = []
            if layer_type in [Layer.TYPES.postgres,Layer.TYPES.spatialite]:
                database_columns = database.get_database_columns(datasource)
                #database_columns = mangle_layer_columns_with_alias(_database_columns,layer_tree)
                joined_columns = get_layer_joined_columns(layer_tree)
                columns = database_columns+joined_columns
            json_columns = json.dumps(columns)
            # Append to layer list
            layers.append({
                    'name': name,
                    'title': title,
                    'is_visible': is_visible,
                    'layer_type': layer_type,
                    'datasource': datasource,
                    'database_columns': json_columns,
                    'order': order+1
                    })

    return layers

def make_datasource(datasource,layer_type):
    new_datasource = None
    # Path and folder name
    base_path = settings.DATASOURCE_PATH.rstrip('/') # eg: /home/sit/charts
    folder = os.path.basename(base_path) # eg: charts
    # OGR example datasource:
    # Original: <datasource>\\SIT-SERVER\sit\charts\definitivo\d262120.shp</datasource>
    # Modified: <datasource>/home/sit/charts\definitivo\d262120.shp</datasource>
    if layer_type == Layer.TYPES.ogr or layer_type == Layer.TYPES.gdal:
        new_datasource = re.sub(r'(.*?)%s(.*)' % folder, r'%s\2' % base_path, datasource) # ``?`` means ungreedy

    # SpatiaLite example datasource:
    # Original: <datasource>dbname='//SIT-SERVER/sit/charts/Carte stradali\\naturalearth_110m_physical.sqlite' table="ne_110m_glaciated_areas" (geom) sql=</datasource>
    # Modified: <datasource>dbname='/home/sit/charts/Carte stradali\\naturalearth_110m_physical.sqlite' table="ne_110m_glaciated_areas" (geom) sql=</datasource>
    if layer_type == Layer.TYPES.spatialite:
        old_path = re.sub(r"(.*)dbname='(.*?)'(.*)", r"\2", datasource) # eg: "//SIT-SERVER/sit/charts/Carte stradali\\naturalearth_110m_physical.sqlite"
        new_path = re.sub(r'(.*?)%s(.*)' % folder, r'%s\2' % base_path, old_path) # eg: "\home\sit\charts/Carte stradali\\naturalearth_110m_physical.sqlite" (``?`` means ungreedy)
        new_datasource = datasource.replace(old_path, new_path)
    
    return new_datasource

def update_qgis_file_datasource(qgis_file):
    """Update qgis file datasource for SpatiaLite and OGR layers.
    
    SpatiaLite and OGR layers need their datasource string to be
    modified at import time so that the original path is replaced with
    the DjangoQGIS one (which is stored in ``settings.py`` as variable
    ``DATASOURCE_PATH``).

    Example original datasource::

    <datasource>\\SIT-SERVER\sit\charts\definitivo\d262120.shp</datasource>

    Example modified datasource::

    <datasource>/home/sit/charts\definitivo\d262120.shp</datasource>
    """

    # Parse the file and build the XML tree
    tree = get_xml_tree(qgis_file)

    # Run through the layer trees
    for layer in tree.xpath('projectlayers/maplayer[@geometry!="No geometry"]'):
        # Get layer details
        if filter_compatible_layertype(layer):
            layer_type = layer.find('provider').text
            datasource = layer.find('datasource').text
    
            new_datasource = make_datasource(datasource,layer_type)
    
            # Update layer
            if new_datasource:
                layer.find('datasource').text = new_datasource

    # Update QGIS file
    with open(qgis_file.path, 'w') as handler:
        tree.write(handler)


def get_qgis_data(qgis_file):
    """Extract data from QGIS file."""

    # Parse file
    tree = get_xml_tree(qgis_file)

    # Extract data
    data = {
        'name': get_project_name(tree),
        'title': get_project_title(tree),
        'initial_extent': get_project_initial_extent(tree),
        'srid': get_project_srid(tree),
        'units': get_project_units(tree),
        'layers': get_project_layers(tree),
        }

    return data


def create_or_update_layer(project, layer_data):
    """Create or update a layer."""

    layer, created = Layer.objects.get_or_create(
        name=layer_data['name'],
        project=project,
        defaults={
            'title': layer_data['title'],
            'is_visible': layer_data['is_visible'],
            'layer_type': layer_data['layer_type'],
            'database_columns': layer_data['database_columns'],
            'datasource': layer_data['datasource'],
            'order': layer_data['order'],
            }
        )
    if not created:
        layer.title = layer_data['title']
        layer.is_visible = layer_data['is_visible']
        layer.layer_type = layer_data['layer_type']
        layer.datasource = layer_data['datasource']
        layer.database_columns = layer_data['database_columns']
        layer.order = layer_data['order']
    # Save layer
    layer.save()


def is_group_compatible(group, qgis_data):
    """Check group and project have same SRID and units."""

    if group.srid != qgis_data['srid']:
        raise Exception(_('Project and group SRID must be the same'))
    if group.units != qgis_data['units']:
        raise Exception(_('Project and group units must be the same'))


def create_qgis_project(qgis_file, qgis_data, group, thumbnail=None, description=None):
    """Create a project with data from a QGIS file."""
    # Extract data from file
    #qgis_data = get_qgis_data(qgis_file) I PARSE IT DURING FORM VALIDATION NOW
    # Create project
    project = Project.objects.create(
        qgis_file=qgis_file,
        group=group,
        title=qgis_data['title'],
        initial_extent=qgis_data['initial_extent'],
        thumbnail=thumbnail,
        description=description,
        )

    # Create or update layers
    for layer in qgis_data['layers']:
        create_or_update_layer(project, layer)

    # Update qgis file datasource for SpatiaLite and OGR layers
    update_qgis_file_datasource(project.qgis_file)

    return project


def update_qgis_project(qgis_file, project, thumbnail=None, description=None):
    """Update a project with data from a QGIS file."""
    qgis_data = get_qgis_data(qgis_file)

    # Check group and project have same SRID and units
    #is_group_compatible(project.group, qgis_data)

    # Update project
    project.qgis_file = qgis_file
    project.title = qgis_data['title']
    project.initial_extent = qgis_data['initial_extent']

    # Update these fields only if not null!
    if thumbnail:
        project.thumbnail = thumbnail
    if description:
        project.description = description

    project.save()
    
        
    # Create or update layers
    for layer in qgis_data['layers']:
        create_or_update_layer(project, layer)

    # Pre-existing layers that have not been updated must be dropped
    new_layer_name_list = [layer['name'] for layer in qgis_data['layers']]
    for layer in project.layer_set.all():
        if layer.name not in new_layer_name_list:
            layer.delete()

    # Update qgis file datasource for SpatiaLite and OGR layers
    update_qgis_file_datasource(project.qgis_file)

    return project
