from django import forms
from django.db import transaction
from django.utils.translation import ugettext, ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.forms import widgets
from django.forms.util import ErrorList
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit, HTML, Button, Row, Field
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions


from .models import Group, Layer, Project, Widget, LayerWidget
from .utils.project_import import get_qgis_data, is_group_compatible

class Validators(object):
    
    @staticmethod
    def validate_qgis_file(qgis_file):
        try:
            qgis_project_data = get_qgis_data(qgis_file)
            qgis_project_title = qgis_project_data['title']
        except Exception as e:
            raise ValidationError(e)
        project_exists = Project.objects.filter(title=qgis_project_title).exists()
        if project_exists:
            raise ValidationError(_('A project with the same title already exists'))

class GroupForm(forms.ModelForm):
    """Group form."""
    def __init__(self, *args, **kwargs):
            super(GroupForm, self).__init__(*args, **kwargs)
            self.helper = FormHelper(self)
            self.helper.form_tag = False
            self.helper.layout = Layout(
                                'name',
                                'title',
                                'description',
                                'srid',
                                'lang',
                                'use_commercial_maps',
                                'units',
                                PrependedText('max_scale','1:'),
                                PrependedText('min_scale','1:'),
                                PrependedText('panoramic_max_scale','1:'),
                                PrependedText('panoramic_min_scale','1:'),
                                'header_logo_img',
                                AppendedText('header_logo_height','px'),
                                PrependedText('header_logo_link','http://'),
                                'header_terms_of_use_text',
                                PrependedText('header_terms_of_use_link','http://'),
                                 )

    class Meta:
        model = Group
        fields = (
            'name',
            'title',
            'description',
            'srid',
            'lang',
            'use_commercial_maps',
            'units',
            'max_scale',
            'min_scale',
            'panoramic_max_scale',
            'panoramic_min_scale',
            'header_logo_img',
            'header_logo_height',
            'header_logo_link',
            'header_terms_of_use_text',
            'header_terms_of_use_link',
            )


class ProjectFormMixin(object):
    def clean_qgis_file(self):
        try:
            qgis_file = self.cleaned_data['qgis_file']
            qgis_project_data = get_qgis_data(qgis_file)
            qgis_project_title = qgis_project_data['title']
            # Check group and project have same SRID and units
            if hasattr(self, 'instance'):
                group = self.instance.group
            else:
                group = self.group
            is_group_compatible(group, qgis_project_data)
        except Exception as e:
            print e
            raise ValidationError(e)
        project_exists = Project.objects.filter(title=qgis_project_title).exists()
        if not isinstance(self, ProjectUpdateForm) and project_exists:
            raise ValidationError(_('A project with the same title already exists'))
        self.qgis_project_data = qgis_project_data
        return qgis_file


class ProjectCreateForm(forms.Form,ProjectFormMixin):
    """Project creation form.

    Ask the user for a valid ``.qgs`` project file and, optionally,
    for a thumbnail image and a description.
    """      
    request = None
    group = None
    
    def __init__(self, *args, **kwargs):
        self.request = kwargs['request']
        del kwargs['request']
        self.group = kwargs['group']
        del kwargs['group']
        super(ProjectCreateForm, self).__init__(*args, **kwargs)
        
    qgis_file = forms.FileField(label=_('Qgis file'))
    thumbnail = forms.ImageField(required=False,label=_('Thumbnail'))
    description = forms.CharField(required=False, widget=forms.Textarea,label=_('Description'))

class ProjectUpdateForm(forms.ModelForm,ProjectFormMixin):
    """Project editing form."""

    class Meta:
        model = Project
        fields = (
            'qgis_file',
            'thumbnail',
            'description',
            )


class LayerUpdateForm(forms.ModelForm):
    """Layer editing form."""

    class Meta:
        model = Layer
        fields = (
            'is_visible',
            )


class ExtendedLayerUpdateForm(forms.ModelForm):
    """Layer editing form with file field."""

    class Meta:
        model = Layer
        fields = (
            'is_visible',
            'data_file',
            )


class WidgetCreateForm(forms.ModelForm):
    """Widget creation form."""

    class Meta:
        model = Widget
        fields = ('name', 'widget_type', 'body')
        widgets = {'body':widgets.HiddenInput}
        
class WidgetUpdateForm(forms.ModelForm):
    """Widget update form."""

    class Meta:
        model = Widget
        fields = ('name', 'body')
        widgets = {'body':widgets.HiddenInput}

