from django.contrib import admin
from django.utils.translation import ugettext, ugettext_lazy as _

from .models import Group, Project, Layer, Widget, LayerWidget


class WidgetAdmin(admin.ModelAdmin):
    pass


class LayerWidgetInline(admin.TabularInline):
    model = LayerWidget
    extra = 1


class LayerAdmin(admin.ModelAdmin):
    inlines = [LayerWidgetInline,]


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('title',)


class GroupAdmin(admin.ModelAdmin):
    list_display = ('title',)


admin.site.register(Layer, LayerAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(Widget, WidgetAdmin)
