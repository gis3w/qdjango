from django.core.management.base import BaseCommand, CommandError, make_option
from django.template import loader
from django.conf import settings
import os
from os.path import abspath, dirname, normpath
from sys import path

APP_ROOT = normpath(dirname(abspath(__file__)) + "/../../../django_qgis/")

class Command(BaseCommand):
    help = 'Write wsgi apache site configuration for this DjangoQgis installation'
    
    option_list = BaseCommand.option_list + (
        make_option('--apache_site_dir',
            action='store',
            dest='apache_site_dir',
            default='/etc/apache2/sites-available/',
            help='Set apache site-available dir, default /etc/apache2/sites-available/'),
        
        make_option('--apache_conf_file',
            action='store',
            dest='apache_conf_file',
            default='djangoqgis.conf',
            help='Set the apache site fiel conf, default djangoqgis.conf'),
                                             
        make_option('--virtualhost',
            action='store',
            dest='virtualhost',
            default=False,
            help='Make a virtualhost and set the web doamin'),
    
        make_option('--daemon',
            action='store',
            dest='daemon',
            default='djangoqgis',
            help='Set wsgi daemon name, default: djangoqgis'),
    
        make_option('--write',
            action='store_true',
            dest='write',
            default=False,
            help='Write apache conf file in site-available apache directory'),
        )
        
    def handle(self, *args, **options):
        # tamplate params
        tParams = options
        tParams['app_root'] = APP_ROOT
        if options['virtualhost']:
            tParams['rooturl'] = '/'
        else:
            tParams['rooturl'] = settings.SITE_URL_PREFIX or "/"
        
        for p in ['MEDIA_ROOT','MEDIA_URL','STATIC_ROOT','STATIC_URL']:
            tParams[p.lower()] = getattr(settings, p)
            
        #fill anche get template rendered
        t = loader.render_to_string('apache.conf.xml',tParams)
            
        #if write option is true:
        # first ew check sudo property for user
        if(options['write']):
            if not 'SUDO_UID' in os.environ.keys():
                raise CommandError("'write' option require super user privileges")
            
            acfile = open(options['apache_site_dir'] + options['apache_conf_file'], 'w')
            acfile.write(t)
            acfile.close()
            
            self.stdout.write('Successfully conf site file apache created in %s' % options['apache_site_dir'])
            
            #try to add conf file
            os.system("a2ensite %s" % options['apache_conf_file'])
            #try to reload
            os.system("service apache2 restart")
            
        else:
            
            self.stdout.write(t)
