DjangoQGIS
==========

DjangoQGIS project aims at bringing new features and possibilities to
QGIS application suite.

`QGIS`_ is a cross-platform (Linux, Windows, Mac) open source
Geographic Information system.

DjangoQGIS web application will address a few questions that go beyond
QGIS goals:

* QGIS project management
* fine-grained access control

.. _QGIS: http://qgis.org

Installation
------------

Start installing Python (2.7), virtualenv and virtualenvwrapper system
wide.

Create a dedicated virtual environment::

  $ mkvirtualenv django-qgis

You can later activate/deactivate the virtualenv with commands::

  $ workon django-qgis
  $ deactivate

Install dependencies within the virtualenv::

  $ pip install -r requirements.txt

If you need to extand your requirements use something like ``-r requirements/production.txt`` where you will import the base requirements and add your own.

To be continued...

Acknowledgements
----------------

* Thanks to Audrey Roy (@audreyr), Daniel Greenfeld (@pydanny) and the
  other people behind `django-twoscoops-project`_; DjangoQGIS makes
  use of that project as a template.

.. _django-twoscoops-project: https://github.com/twoscoops/django-twoscoops-project/
